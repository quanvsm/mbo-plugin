<?php
$plugin_dir = ABSPATH . 'wp-content/plugins/MBO-Plugin/';
require_once $plugin_dir . 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;

if (isset($_FILES["csv_file"])) {
    //empty uploads folder
    $file_pointer = glob($plugin_dir . '/uploads/*');
    foreach ($file_pointer as $file) { // iterate files
        if (is_file($file)) {
            unlink($file); // delete file
        }
    }

    //Move file to upload folder
    $uploads_dir = $plugin_dir . '/uploads/';
    $tmp_name = $_FILES["csv_file"]["tmp_name"];
    $name = basename($_FILES["csv_file"]["name"]);
    if (!file_exists($uploads_dir . "/" . $name)) {
        move_uploaded_file($tmp_name, "$uploads_dir/$name");
    }

    if (isset($_SESSION['vsm_user_data_file_name'])) {
        unset($_SESSION['vsm_user_data_file_name']);
    }
    if (isset($_SESSION['vsm_user_contact_file_name'])) {
        unset($_SESSION['vsm_user_contact_file_name']);
    }

    $_SESSION['vsm_user_data_file_name'] = $name;

    $spreadsheet = new Spreadsheet();

    $inputFileType = 'Xlsx';
    $inputFileName = $uploads_dir . $name;

    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    $reader->setReadDataOnly(true);
    $reader->setReadEmptyCells(false);

    $worksheetData = $reader->listWorksheetInfo($inputFileName);

    $row = 1;

    $user_id = "user_" . get_current_user_id();
    $mbo_1 =  get_field("mbo_1", $user_id);
    $mbo_2 =  get_field("mbo_2", $user_id);
    $mbo_3 =  get_field("mbo_3", $user_id);
}

if (isset($_FILES["user_contact"])) {
    //Empty Uploads Folder.
    $file_pointer = glob($plugin_dir . '/uploads/*');
    foreach ($file_pointer as $file) {
        if (is_file($file)) {
            unlink($file);
        }
    }

    //Move file to uploads foler.
    $uploads_dir = $plugin_dir . '/uploads/';
    $tmp_name = $_FILES["user_contact"]["tmp_name"];
    $name = basename($_FILES["user_contact"]["name"]);
    if (!file_exists($uploads_dir . "/" . $name)) {
        move_uploaded_file($tmp_name, "$uploads_dir/$name");
    }

    if (isset($_SESSION['vsm_user_data_file_name'])) {
        unset($_SESSION['vsm_user_data_file_name']);
    }
    if (isset($_SESSION['vsm_user_contact_file_name'])) {
        unset($_SESSION['vsm_user_contact_file_name']);
    }

    $_SESSION['vsm_user_contact_file_name'] = $name;

    $spreadsheet = new Spreadsheet();

    $inputFileType = 'Xlsx';
    $inputFileName = $uploads_dir . $name;

    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    $reader->setReadDataOnly(true);
    $reader->setReadEmptyCells(false);

    $worksheetData = $reader->listWorksheetInfo($inputFileName);

    $row = 1;

    $user_id = "user_" . get_current_user_id();
}
?>

<?php if (isset($_SESSION['error'])) : ?>
    <div class="notice notice-error inline">
        <p><?= $_SESSION["error"]; ?></p>
        <?php unset($_SESSION["error"]); ?>
    </div>
<?php endif; ?>
<?php if (isset($_SESSION['success'])) : ?>
    <div class="notice notice-success inline">
        <p><?= $_SESSION['success'] ?></p>
        <?php unset($_SESSION["success"]); ?>
    </div>
<?php endif; ?>

<?php if (isset($_SESSION['warning'])) : ?>
    <div class="notice notice-warning inline">
        <p>Imported <?= $_SESSION["warning"]; ?> rows, Please check account number!</p>
        <?php unset($_SESSION["warning"]); ?>
    </div>
<?php endif; ?>

<?php if (isset($_SESSION['delete-warning'])) : ?>
    <div class="notice notice-warning inline">
        <p><?= $_SESSION["delete-warning"]; ?> , Please check account number!</p>
        <?php unset($_SESSION["delete-warning"]); ?>
    </div>
<?php endif; ?>

<section>
    <h1>Import functions</h1>
    <div style="display: flex; justify-content: space-around;flex-wrap: wrap; border: 1px #c3c4c7 solid;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px; margin: 50px; padding: 30px">
        <div class="card" style=" box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px; margin: 20px auto">
            <h2>VSM Import Data</h2>
            <fieldset>
                <form method="POST" action="" enctype="multipart/form-data">
                    <input required class="file" type="file" name="csv_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="custom-file-input">
                    <input name="preview_user_data" type="submit" class="button-primary" value="Preview">
                </form>
            </fieldset>
        </div>
        <div class="card" style=" box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px;margin: 20px auto">
            <h2>VSM Import Contact Information</h2>
            <fieldset>
                <form method="POST" action="" enctype="multipart/form-data">
                    <input required class="file" type="file" name="user_contact" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="custom-file-input">
                    <input name="preview_user_contact_data" type="submit" class="button-primary" value="Preview">
                </form>
            </fieldset>
        </div>
    </div>
</section>

<section>
    <h1>Delete User Data</h1>
    <form method="POST" action="" id="delete_data_form" enctype="multipart/form-data" style="display: flex; justify-content: space-around; flex-wrap: wrap;border: 1px #c3c4c7 solid;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px; margin: 50px; padding: 30px">
        <!-- <div class="card" style=" box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px;margin: 20px auto"> -->
        <!-- <h2>Clear User Data</h2> -->
        <div style="display: flex;flex-direction:column;justify-content:space-between;">
            <h3>Account numbers</h3>
            <textarea required style="width:100%;" name="user_account_numbers" placeholder="Enter account numbers (separated by comma)" id="user_account_numbers" cols="80" rows="6"></textarea>
        </div>

        <div class="checklist" style="width:200px;max-height: 200px;display: flex;flex-direction:column;justify-content:space-between;">
            <h3>Select data type</h3>
            <label for=""><input type="checkbox" name="data_type[]" id="data_type_checkboxes" value="contact-info">Contact Info</label>
            <div style="display: flex;justify-content:space-between;">
                <label style="width: 100px;"><input type="checkbox" name="data_type[]" value="mbo-1">MBO I</label>
                <label style="width: 100px;"><input type="checkbox" name="data_type[]" value="mbo-2">MBO II</label>
            </div>
            <div style="display: flex;justify-content:space-between;">
                <label style="width: 100px;"><input type="checkbox" name="data_type[]" value="mbo-3">MBO III</label>
                <label style="width: 100px;"><input type="checkbox" name="data_type[]" value="mbo-4">MBO IV</label>
            </div>
        </div>
        <!-- </div> -->
        <input style="display: none;" id="clear-data-submit-button" name="clear_user_data" value="Clear" type="submit" class="button-primary">
        <a href="#TB_inline?width=120&height=100&inlineId=modal-window-id" class="button-primary thickbox" id="clear-user-data-button" style="max-height: 30px; margin-top:50px;" class="">Clear User Data</a>
    </form>
</section>
<?php add_thickbox(); ?>

<div id="modal-window-id" style="display:none; text-align:center;">
    <h3>Are you sure you want to delete data for selecetd users ?</h3>
    <button id="accpet-delete-data-button" class="button-primary">Proceed</button>
</div>

<script>
    jQuery(document).ready(function($) {
        $("#accpet-delete-data-button").click(function() {
            $("#clear-data-submit-button").click();

        })
    });
</script>

<!-- Preview for User Data -->
<?php if (isset($worksheetData) && isset($_SESSION['vsm_user_data_file_name'])) : ?>
    <section style="border: 1px #c3c4c7 solid;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px;margin: 30px auto;padding: 30px;">
        <h3>Preview</h3>
        <div style="display: flex; flex-wrap: wrap;flex-direction: row;justify-content:space-around">
            <?php foreach ($worksheetData as $worksheet) : ?>
                <?php
                $sheetName = $worksheet['worksheetName'];
                $reader->setLoadSheetsOnly($sheetName);
                $spreadsheet = $reader->load($inputFileName);
                $worksheet = $spreadsheet->getActiveSheet();
                $data = $worksheet->toArray();
                ?>
                <?php for ($x = 1; $x < count($data); $x++) : ?>
                    <div class="card" style="width: 24%;border: 1px #c3c4c7 solid;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;border-radius: 5px; margin: 20px auto; padding: 30px;">
                        <ul>
                            <?php for ($i = 0; $i < count($data[$x]) - 1; $i++) : ?>
                                <li><strong><?= $data[0][$i] ?></strong> : <?= $data[$x][$i] ?></li>
                            <?php endfor; ?>
                        </ul>
                        <span style="float: right;font-size:11px;font-weight:bolder;"><strong><?= $x; ?></strong></span>
                    </div>
                    <?php $row++; ?>
                <?php endfor; ?>
            <?php endforeach; ?>
        </div>
        <fieldset>
            <form method="POST" action="">
                <div>
                    <input name="update" type="submit" value="Update" class="button-primary">
                    <input name="cancel" type="submit" value="Cancel" class="button-secondary">
                </div>
            </form>
        </fieldset>
    </section>
<?php endif; ?>


<!-- Preview for User contact -->
<?php if (isset($worksheetData) && isset($_SESSION['vsm_user_contact_file_name'])) : ?>
    <section style="border: 1px #c3c4c7 solid;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;  border-radius: 5px;margin: 30px auto; padding: 20px;">
        <h3>Preview</h3>
        <div style="display: flex; flex-wrap: wrap;flex-direction: row;justify-content:space-around;">
            <?php foreach ($worksheetData as $worksheet) : ?>
                <?php
                $sheetName = $worksheet['worksheetName'];
                $reader->setLoadSheetsOnly($sheetName);
                $spreadsheet = $reader->load($inputFileName);
                $worksheet = $spreadsheet->getActiveSheet();
                $data = $worksheet->toArray();
                ?>
                <?php for ($x = 1; $x < count($data); $x++) : ?>
                    <div class="card" style="width: 24%;border: 1px #c3c4c7 solid;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;border-radius: 5px;margin:30px auto;">
                        <ul>
                            <?php for ($i = 0; $i < count($data[$x]); $i++) : ?>
                                <li><strong><?= $data[0][$i] ?></strong> : <?= $data[$x][$i] ?></li>
                            <?php endfor; ?>
                        </ul>
                        <span style="float: right;font-size:11px;"><strong><?= $x; ?></strong></span>
                    </div>
                    <?php $row++; ?>
                <?php endfor; ?>
            <?php endforeach; ?>
        </div>
        <fieldset>
            <form method="POST" action="">
                <div>
                    <input name="update_contact" type="submit" value="Update" class="button-primary">
                    <input name="cancel" type="submit" value="Cancel" class="button-secondary">
                </div>
            </form>
        </fieldset>
    </section>
<?php endif; ?>