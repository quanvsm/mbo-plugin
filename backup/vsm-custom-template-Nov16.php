<?php
    /*
    NOTES: 

     search "mbo1" to jump to mbo 1 Financial Indicators section , same with mbo2 and mbo3
     search "mbo1-returns" to jump to mbo 1 Returns & General Reporting section, same with mbo2 and mbo3

    */
    $user_id = "user_".get_current_user_id();
    $user_data_post_id =  get_field("user_account_number",$user_id);
    $post = get_page_by_title($user_data_post_id, OBJECT, 'user-data');

    //mbo and company information
    $mbo_1 =  get_field("mbo_1",$user_id);
    $mbo_2 =  get_field("mbo_2",$user_id);
    $mbo_3 =  get_field("mbo_3",$user_id);
    $mbo_4 =  get_field("mbo_4",$user_id);
    $mbo_1_company = get_field('mbo_1_company',$user_id);
    $mbo_2_company = get_field('mbo_2_company',$user_id);
    $mbo_3_company = get_field('mbo_3_company',$user_id);
    $mbo_4_company = get_field('mbo_4_company',$user_id);
    
    //Get user contact information
    $name =  get_field("name",$post->ID);
    $address =  get_field("address",$post->ID);
    $contact_name_1 =  get_field("contact_name_1",$post->ID);
    $contact_name_2 =  get_field("contact_name_2",$post->ID);
    $phone_number =  get_field("phone_number",$post->ID);
    $email =  get_field("email",$post->ID);

    //Stylesheet path
    $css_path =  plugins_url('/css/style.min.css', __FILE__);
    $logo_path =  plugins_url('/img/Westcap.png', __FILE__);
    $background_path =  plugins_url('/img/reduced.png', __FILE__);

    //get return data
    if($user_data_post_id == '830820755'){
      $return_report_post = get_page_by_title("User Portal 830820755", OBJECT, 'page');
    }
    else{
      $return_report_post = get_page_by_title("User Portal", OBJECT, 'page');
    }

    //Function to display Financial Indicator Table
    // $mbo : Fund number  (1,2,3,4)
    // $post : Post id of user data
    // $company: company (1,2,3,4) 
    function displayMbo($mbo, $post,$company){
        $comp = "c".$company;
        if($mbo ==1 ){
          $as_of_field_name = "as_of";
        }
        else{
          $as_of_field_name= "as_of_".$mbo;
        }
        $as_of = get_field($as_of_field_name,$post->ID);
        if( have_rows($comp.'_mbo_'.$mbo) ):
          while( have_rows($comp.'_mbo_'.$mbo) ): the_row();
          

          //Annual Statements
          $annual_statement_link= [];
          $annual_statement_year= [];
          if( have_rows($comp.'_annual_statements_'.$mbo,$post->ID) ):
            while( have_rows($comp.'_annual_statements_'.$mbo,$post->ID) ) : the_row();
                $annual_statement_year[] = get_sub_field($comp."_annual_statement_year_".$mbo,$post->ID);
                $annual_statement_link[] =get_sub_field($comp.'_annual_statement_link_'.$mbo,$post->ID);
              endwhile;
            endif;

            if($mbo >1 ){

              //Cash Call
              $cash_call_letter_link= [];
              $cash_call_year= [];
              if( have_rows($comp.'_cash_call_'.$mbo,$post->ID) ):
                while( have_rows($comp.'_cash_call_'.$mbo,$post->ID) ) : the_row();
                    $cash_call_year[] = get_sub_field($comp."_cash_call_date_".$mbo,$post->ID);
                    $cash_call_letter_link[] =get_sub_field($comp.'_capital_call_letter_link_'.$mbo,$post->ID);
                  endwhile;
                endif;

                if($mbo >= 3){
                  $closing_binder_title= [];
                  $closing_binder_link= [];
                  if( have_rows($comp.'_closing_binder_'.$mbo,$post->ID) ):
                    while( have_rows($comp.'_closing_binder_'.$mbo,$post->ID) ) : the_row();
                        $closing_binder_title[] = get_sub_field($comp."_closing_binder_title_".$mbo,$post->ID);
                        $closing_binder_link[] =get_sub_field($comp.'_closing_binder_link_'.$mbo,$post->ID);
                      endwhile;
                    endif;
                }
            }

          //Tax Slip
          $tax_year= [];
          $tax_link= [];
          if( have_rows($comp.'_tax_slips_'.$mbo,$post->ID) ):
            while( have_rows($comp.'_tax_slips_'.$mbo,$post->ID) ) : the_row();
                  $tax_year[] = get_sub_field($comp."_tax_year_".$mbo,$post->ID);
                  $tax_link[] =get_sub_field($comp.'_tax_file_link_'.$mbo,$post->ID);
              endwhile;
            endif;

            $company_name= get_sub_field($comp."_company_name_".$mbo,$post->ID);

          $units = number_format(get_sub_field($comp."_units_".$mbo,$post->ID), 0, ".", "," );
          $commiteed_capital = number_format(get_sub_field($comp."_committed_capital_".$mbo,$post->ID), 0, ".", "," );
          $contributed_capital = number_format(get_sub_field($comp."_contributed_capital_".$mbo,$post->ID), 0, ".", "," );
          $cumulative_distributions = number_format(get_sub_field($comp."_cumulative_distributions_".$mbo,$post->ID), 0, ".", "," );
          $net_asset_value = number_format(get_sub_field($comp."_net_asset_value_".$mbo,$post->ID), 0, ".", "," );
          $net_gain_loss = number_format(get_sub_field($comp."_net_gains_loss_".$mbo,$post->ID), 0, ".", "," );

          $beginning_balance_date= get_sub_field($comp."_beginning_balance_date_".$mbo,$post->ID);
          $ending_balance_date= get_sub_field($comp."_ending_balance_date_".$mbo,$post->ID);
  
          $beginning_balance = number_format(get_sub_field($comp."_beginning_balance_".$mbo,$post->ID), 0, ".", "," );
          $capital_calls = number_format(get_sub_field($comp."_capital_calls_".$mbo,$post->ID), 0, ".", "," );
          $in_de_value = number_format(get_sub_field($comp."_increase_decrease_in_value_".$mbo,$post->ID), 0, ".", "," );
          $distributions = number_format(get_sub_field($comp."_distributions_".$mbo,$post->ID), 0, ".", "," );
          $ending_balance = number_format(get_sub_field($comp."_ending_balance_".$mbo,$post->ID), 0, ".", "," );
        endwhile;
      endif;

      $mbo_html = '<div class="section">';
      if($company_name){
        $mbo_html .=  ' <h2 class="mbo-'.$mbo.'">Financial Indicators - '.$company_name.'</h2>';
      }else{
        $mbo_html .=  ' <h2 class="mbo-'.$mbo.'">Financial Indicators</h2>';
      }
      
      $mbo_html .=  '       <div class="tab-container">';
      $mbo_html .=  '         <span class="tab">Summary</span>';
      $mbo_html .=  '       <div class="content"> ';
      if($as_of){
        $mbo_html .=  '          <h3>As of '.$as_of.'</h3> ';
      }
      $mbo_html .=  ' <table>';
      $mbo_html .=  '   <thead> ';
      $mbo_html .=  '     <tr>';
      $mbo_html .=  '         <th><strong>Units</strong></th>';
      $mbo_html .=  '         <th><strong>Committed Capital</strong></th>';
      $mbo_html .=  '         <th><strong>Contributed Capital</strong></th>';
      $mbo_html .=  '         <th><strong>Cumulative Distributions</strong></th>';
      $mbo_html .=  '         <th><strong>Net Asset Value</strong></th>';
      $mbo_html .=  '         <th><strong>Net Gain on Contributed Capital</strong></th>';
      $mbo_html .=  '     </tr>';
      $mbo_html .=  '    </thead>';
      $mbo_html .=  '     <tbody>';
      $mbo_html .=  '           <tr>';
      $mbo_html .=  '                 <td>'.$units.'</td>';
      $mbo_html .=  '                 <td>$'.$commiteed_capital.'</td>';
      $mbo_html .=  '                 <td>$'.$contributed_capital.'</td>';
      $mbo_html .=  '                 <td>$'.$cumulative_distributions.'</td>';
      $mbo_html .=  '                 <td>$'.$net_asset_value.'</td>';
      $mbo_html .=  '                 <td>$'.$net_gain_loss.'</td>';
      $mbo_html .=  '            </tr>';
      $mbo_html .=  '      </tbody>';
      $mbo_html .=  '</table>';
      $mbo_html .=  '<table>';
      $mbo_html .=  '  <thead>';
      $mbo_html .=  '      <tr>';
      $mbo_html .=  '                 <th><strong>Beginning Value ('.$beginning_balance_date.')</strong></th>';
      $mbo_html .=  '                 <th><strong>Capital Calls</strong></th>';
      $mbo_html .=  '                 <th><strong>Increase in Value</strong></th>';
      $mbo_html .=  '                 <th><strong>Distributions</strong></th>';
      $mbo_html .=  '                 <th><strong>Ending Value ('.$ending_balance_date.')</strong></th>';
      $mbo_html .=  '       </tr>';
      $mbo_html .=  '  </thead>';
      $mbo_html .=  '  <tbody> ';
      $mbo_html .=  '  <tr>';
      $mbo_html .=  '         <td>$'.$beginning_balance.'</td>';
      $mbo_html .=  '         <td>$'.$capital_calls.'</td>';
      $mbo_html .=  '         <td>$'.$in_de_value.'</td>';
      $mbo_html .=  '         <td>$'.$distributions.'</td>';
      $mbo_html .=  '         <td>$'.$ending_balance.'</td>';
      $mbo_html .=  '   </tr>';
      $mbo_html .=  '    </tbody>';
      $mbo_html .=  '  </table>';
      $mbo_html .=  '         <br><p>See annual statement for detailed description of each value.</p>';
      $mbo_html .=  ' </div>';

      //Annual Statement Tab
      $mbo_html .=  '         <span class="tab">Annual Statements</span>';
      $mbo_html .=  '         <div class="content">';
      $mbo_html .=  '         <table>';
      $mbo_html .=  '         <thead>';
      $mbo_html .=  '               <tr>';
      $mbo_html .=  '                 <th><strong>Year</strong></th>';
      $mbo_html .=  '                 <th><strong>Download</strong></th>';
      $mbo_html .=  '              </tr> ';
      $mbo_html .=  '             </thead>';
      $mbo_html .=  '            <tbody> ';
      for( $i =0 ;$i < count($annual_statement_year) ; $i++){
          $mbo_html .=  '  <tr>';
          $mbo_html .=  ' <td>'.$annual_statement_year[$i].'</td>';
          $mbo_html .=  ' <td><a href='.$annual_statement_link[$i].'>Download</a></td>';
          $mbo_html .=  '  </tr>';
      }
      $mbo_html .=  ' </tbody>';
      $mbo_html .=  '  </table>';
      $mbo_html .=  '    </div>';

      //cash call tab
      if($mbo > 1){
        $mbo_html .=  '<span class="tab">Cash Calls</span>';
        $mbo_html .=  '<div class="content">';
        $mbo_html .=  '<table>';
        $mbo_html .=    '<thead>';
        $mbo_html .=     ' <tr>';
        $mbo_html .=        '<th><strong>Cash Call Date</strong></th>';
        $mbo_html .=       ' <th><strong>Capital Call Letter</strong></th>';
        $mbo_html .=      '</tr>';
        $mbo_html .=     '</thead>';
        $mbo_html .=    '<tbody>';
        for( $i =0 ;$i < count($cash_call_year) ; $i++){
          $mbo_html .=  '  <tr>';
          $mbo_html .=  ' <td>'.$cash_call_year[$i].'</td>';
          $mbo_html .=  ' <td><a href='.$cash_call_letter_link[$i].'>Download</a></td>';
          $mbo_html .=  '  </tr>';
      }
        $mbo_html .=   '</tbody>';
        $mbo_html .=  '</table>';
        $mbo_html .=   '</div>';
      }

      //Tax slip tab
      $mbo_html .=  ' <span class="tab">Tax Slips</span>';
      $mbo_html .=  ' <div class="content">';
      $mbo_html .=  '  <table>';
      $mbo_html .=  '  <thead>';
      $mbo_html .=  '  <tr>';
      $mbo_html .=  ' <th><strong>Year</strong></th>';
      $mbo_html .=  ' <th><strong>Download</strong></th>';
      $mbo_html .=  '   </tr>';
      $mbo_html .=  '  </thead>';
      $mbo_html .=  '            <tbody> ';
      for( $i =0 ;$i < count($tax_year) ; $i++){
          $mbo_html .=  '  <tr>';
          $mbo_html .=  ' <td>'.$tax_year[$i].'</td>';
          $mbo_html .=  ' <td><a href='.$tax_link[$i].'>Download</a></td>';
          $mbo_html .=  '  </tr>';
      }
      $mbo_html .=  '             </tbody>';
      $mbo_html .=  '           </table>';
      $mbo_html .=  '         </div>'; 

      //closing binder yab
      if($mbo >= 3){
        $mbo_html .=  '<span class="tab">Closing Binder</span>';
        $mbo_html .=  '<div class="content">';
        $mbo_html .=  '<table>';
        $mbo_html .=    '<thead>';
        $mbo_html .=     ' <tr>';
        $mbo_html .=        '<th><strong>Title</strong></th>';
        $mbo_html .=       ' <th><strong>Download</strong></th>';
        $mbo_html .=      '</tr>';
        $mbo_html .=     '</thead>';
        $mbo_html .=    '<tbody>';
        for( $i =0 ;$i < count($closing_binder_title) ; $i++){
          $mbo_html .=  '  <tr>';
          $mbo_html .=  ' <td>'.$closing_binder_title[$i].'</td>';
          $mbo_html .=  ' <td><a href='.$closing_binder_link[$i].'>Download</a></td>';
          $mbo_html .=  '  </tr>';
      }
        $mbo_html .=   '</tbody>';
        $mbo_html .=  '</table>';
        $mbo_html .=   '</div>';
      }

      $mbo_html .=  '       </div>';
      $mbo_html .=  '     </div>'; 

      return $mbo_html;
    }


    function displayReturnAndGeneralReporting($mbo , $post,$mmbo_post){

      $annual_cash_yield_year= [];
      $annual_cash_yield_value= [];
      if( have_rows('annual_cash_yield_'.$mbo,$post->ID) ):
        while( have_rows('annual_cash_yield_'.$mbo,$post->ID) ) : the_row();
            $annual_cash_yield_year[] = get_sub_field("annual_cash_yield_year_".$mbo,$post->ID);
            $annual_cash_yield_value[] =get_sub_field('annual_cash_yield_value_'.$mbo,$post->ID);
          endwhile;
        endif;

        $notice_of_distribution_year= [];
        $notice_of_distribution_year= [];
        if( have_rows('notice_of_distribution_'.$mbo,$post->ID) ):
          while( have_rows('notice_of_distribution_'.$mbo,$post->ID) ) : the_row();
              $notice_of_distribution_year[] = get_sub_field("notice_of_distribution_year_".$mbo,$post->ID);
              $notice_of_distribution_link[] =get_sub_field('notice_of_distribution_link_'.$mbo,$post->ID);
            endwhile;
          endif;

          $net_return_repeater_label= [];
          $net_return_repeater_value= [];
          if( have_rows('net_return_'.$mbo,$post->ID) ):
            while( have_rows('net_return_'.$mbo,$post->ID) ) : the_row();
                $net_return_repeater_label[] = get_sub_field("net_return_label_".$mbo,$post->ID);
                $net_return_repeater_value[] =get_sub_field('net_return_value_'.$mbo,$post->ID);
              endwhile;
            endif;

          if($mbo ==1 ){
            $as_of_field_name = "as_of";
          }
          else{
            $as_of_field_name= "as_of_".$mbo;
          }
        $as_of = get_field($as_of_field_name,$mmbo_post->ID);

        // $net_return_1= get_field("net_return_1_year_".$mbo,$post->ID);
        // $net_return_3= get_field("net_return_3_year_".$mbo,$post->ID);
        // $net_return_5= get_field("net_return_5_year_".$mbo,$post->ID);
        // $net_return_inception= get_field("net_return_inception_".$mbo,$post->ID);

        $General_report =  '<div class="section">';
        $General_report .= '<h2 class="mbo-'.$mbo.'">Returns & General Reporting</h2>';
        $General_report .= '<div class="tab-container">';
        $General_report .= '<span class="tab">RETURNS</span>';
        $General_report .= '<div class="content">';
        // if($as_of){
        //   $General_report .= '<h3>Net Returns as of '.$as_of.'<sup style="font-size: 12px;">8</sup></h3>';
        // }
        // $General_report .= '<table>';
        // $General_report .= '<thead>';
        // $General_report .=         '<tr>';
        // $General_report .=          ' <th><strong>1 YEAR</strong></th>';
        // $General_report .=         '<th><strong>3 YEAR</strong></th>';
        // $General_report .=        ' <th><strong>5 YEAR</strong></th>';
        // $General_report .=         '<th><strong>SINCE INCEPTION</strong></th>';
        // $General_report .=    '  </tr>';
        // $General_report .=    ' </thead>';
        // $General_report .=  '  <tbody>';
        // $General_report .=      '<tr>';
        // $General_report .=       '<td>'.$net_return_1.'%</td>';
        // $General_report .=       '<td>'.$net_return_3.'%</td>';
        // $General_report .=     '<td>'.$net_return_5.'%</td>';
        // $General_report .=     '<td>'.$net_return_inception.'%</td>';
        // $General_report .=     ' </tr>';
        // $General_report .=      '</tbody>';
        // $General_report .=     '</table>';
        //New Net return table
        if($as_of){
          $General_report .= '<h3>Net Returns as of '.$as_of.'<sup style="font-size: 12px;">8</sup></h3>';
        }
        else{
          $General_report .= '<h3>Net Returns <sup style="font-size: 12px;">8</sup></h3>';
        }
        $General_report .=  '<table style="width:auto;">';
        for( $i =0 ;$i < count($net_return_repeater_label) ; $i++){
          $General_report .=  '  <tr>';
          $General_report .=  ' <th>'.$net_return_repeater_label[$i].'</th>';
          $General_report .=  ' <td>'.$net_return_repeater_value[$i].'%</td>';
          $General_report .=  '  </tr>';
      }
        $General_report .= ' </table>';
      //end
        $General_report .=  '<h3 style="padding-top: 15px;">Annualized Cash Yield<sup style="font-size: 12px;">9</sup></h3>';
        $General_report .=  '<table style="width:auto;">';
        for( $i =0 ;$i < count($annual_cash_yield_year) ; $i++){
          $General_report .=  '  <tr>';
          $General_report .=  ' <th>'.$annual_cash_yield_year[$i].'</th>';
          $General_report .=  ' <td>'.$annual_cash_yield_value[$i].'%</td>';
          $General_report .=  '  </tr>';
      }
        $General_report .= ' </table>';
        $General_report .=  '<div style="padding-top: 20px;">';
        $General_report .=    '<p><sup>8</sup> This represents the internal rate of return (IRR) for the respective period. IRR is a money weighted method to calculate rates of return.</p> ';
        $General_report .=   '<p><sup>9</sup> Based on operating year and invested capital.</p> ';
        $General_report .=   '</div>';
        $General_report .=  '</div>';
        $General_report .= '<span class="tab">DISTRIBUTIONS</span>';
        $General_report .=  '<div class="content">';
        $General_report .=       '<table>';
        $General_report .=      ' <thead>';
        $General_report .=        '<tr>';
        $General_report .=        '<th><strong>Year</strong></th>';
        $General_report .=        '<th><strong>Download</strong></th>';
        $General_report .=     '</tr>';
        $General_report .=   '</thead>';
        $General_report .=   '<tbody>';
          for( $i =0 ;$i < count($notice_of_distribution_year) ; $i++){
            $General_report .=  '  <tr>';
            $General_report .=  ' <td>'.$notice_of_distribution_year[$i].'</td>';
            $General_report .=  ' <td><a href='.$notice_of_distribution_link[$i].'>Download</a></td>';
            $General_report .=  '  </tr>';
        }
        $General_report .=   '</tbody>';
        $General_report .=   '</table>';
        $General_report .=   '</div>';

        $General_report .=   '<span class="tab">REPORTS</span>';
        $General_report .=  '<div class="content" >';
        $General_report .=   '<table>';
        $General_report .=   ' <thead>';
        $General_report .=      '<tr>';
        $General_report .=      '  <th><strong>Year</strong></th>';
        $General_report .=      ' <th><strong>Download</strong></th>';
        $General_report .=   '  </tr>';
        $General_report .=  '  </thead>';
        $General_report .=   ' <tbody>';
          if( have_rows('investor_reports_'.$mbo,$post->ID) ):
           while( have_rows('investor_reports_'.$mbo,$post->ID) ) : the_row();
           $General_report .=        '<tr>';
           $General_report .=        ' <td>'.get_sub_field('report_year_'.$mbo).'</td>';
           $General_report .=         get_sub_field('q1_report_link_'.$mbo) ? '<td><a href='. get_sub_field('q1_report_link_'.$mbo,$post->ID).'> Q1 Report</a></td>' : '<td>Q1 Report</td>';    
           $General_report .=         get_sub_field('q2_report_link_'.$mbo) ? ('<td><a href='. get_sub_field('q2_report_link_'.$mbo,$post->ID).'> Q2 Report</a></td>') : '<td>Q2 Report</td>';
           $General_report .=         get_sub_field('q3_report_link_'.$mbo) ? '<td><a href='. get_sub_field('q3_report_link_'.$mbo,$post->ID).'> Q3 Report</a></td>' : '<td>Q3 Report</td>';
           $General_report .=         get_sub_field('annual_report_link_'.$mbo) ? '<td><a href='. get_sub_field('annual_report_link_'.$mbo,$post->ID).'> Annual Report</a></td>' : '<td>Annual Report</td>';
           $General_report .=    '</tr>';
              endwhile;
            endif;
        $General_report .=   ' </tbody>';
        $General_report .=     '</table>';
        $General_report .=  ' </div>';

        $General_report .=  '<span class="tab">AGM & NEWS</span>';
        $General_report .=  '<div class="content">';
        $General_report .=    '<table>';
        $General_report .=     '<thead>';
        $General_report .=      '<tr>';
        $General_report .=       ' <th><strong>Year</strong></th>';
        $General_report .=       '<th><strong>Download</strong></th>';
        $General_report .=     '</tr>';
        $General_report .=  '</thead>';
        $General_report .=  '<tbody>';
        if( have_rows('agm_docs_'.$mbo,$post->ID) ):
          while( have_rows('agm_docs_'.$mbo,$post->ID) ) : the_row();
          $General_report .=     '<tr>';
          $General_report .=        '<td>'.get_sub_field('agm_year_'.$mbo).'</td>';
          if( have_rows('agm_documents_'.$mbo,$post->ID) ):
            while( have_rows('agm_documents_'.$mbo,$post->ID) ) : the_row();
            $General_report .=        '<td><a href="'. get_sub_field('agm_document_link_'.$mbo,$post->ID).'">Download</a></td>';
          endwhile;
        endif;
            $General_report .= '</tr>';
          endwhile;
        endif;
        $General_report .=  '</tbody>';
        $General_report .=  '</table>';
        $General_report .=  '</div>';
        $General_report .=   '</div>';
        $General_report .=  '</div>';

        return $General_report;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Westcap</title>
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href=<?=$css_path?>>
</head>
<body>
  
  <div id="header">
    <div class="container">
      <img class="logo" src=<?= $logo_path?> width="695" height="310" alt="Westcap">
      <nav>
        <a href="/">Home</a>
        <a href="/user-portal">Investor Details</a>
        <a href="/contact-us">Contact Us</a>
        <a href="<?php echo wp_logout_url(get_permalink()); ?>">Logout</a>
      </nav>
      <span class="hamburger"></span>
    </div>
  </div>
  
  <div id="main">
    <div class="section page-header">
      <div class="container">
        <h1>Investor Details</h1>
      </div>
    </div>
    
    <div class="section page-sub-header">
      <div class="container">
        <div>
          <p><strong>SUBSCRIBER:</strong></p>
          <p><?=$name?></p>
          <p><strong>CONTACT: </strong></p>
          <p><?=$address?></p>
          <p><?=$contact_name_1?></p>
          <p><?=$contact_name_2?></p>
        </div>
        <div>
          <p><strong>PHONE NUMBER: </strong></p>
          <p><?=$phone_number?></p>
          <p><strong>EMAIL ADDRESS: </strong></p>
          <p><?=$email?></p>
        </div>
      <div>
        <a href="/contact-us" class="button">Contact Us</a>
          <a href="/password-reset" class="button" target="_blank">Password Reset</a>
        </div>
      </div>
    </div>

<?php 
  if($mbo_1): 
  if($mbo_1_company && in_array('MBO I - C1', $mbo_1_company)): 
    echo displayMbo(1,$post,1);
    endif;
    if($mbo_1_company && in_array('MBO I - C2', $mbo_1_company)): 
      echo displayMbo(1,$post,2);
      endif;
      if($mbo_1_company && in_array('MBO I - C3', $mbo_1_company)): 
        echo displayMbo(1,$post,3);
        endif;
        if($mbo_1_company && in_array('MBO I - C4', $mbo_1_company)): 
          echo displayMbo(1,$post,4);
          endif;
  echo displayReturnAndGeneralReporting(1,$return_report_post,$post);
  endif;
  ?>

<?php 
     if($mbo_2): 
      if($mbo_2_company && in_array('MBO II - C1', $mbo_2_company)):  
        echo displayMbo(2,$post,1);
        endif;
        if($mbo_2_company && in_array('MBO II - C2', $mbo_2_company)): 
          echo displayMbo(2,$post,2);
          endif;
          if($mbo_2_company && in_array('MBO II - C3', $mbo_2_company)): 
            echo displayMbo(2,$post,3);
            endif;
            if($mbo_2_company && in_array('MBO II - C4', $mbo_2_company)): 
              echo displayMbo(2,$post,4);
              endif;
      echo displayReturnAndGeneralReporting(2,$return_report_post,$post);
      endif;
     ?>

<?php 
     if($mbo_3): 
      if($mbo_3_company && in_array('MBO III - C1', $mbo_3_company)): 
        echo displayMbo(3,$post,1);
        endif;
        if($mbo_3_company && in_array('MBO III - C2', $mbo_3_company)): 
          echo displayMbo(3,$post,2);
          endif;
          if($mbo_3_company && in_array('MBO III - C3', $mbo_3_company)): 
            echo displayMbo(3,$post,3);
            endif;
            if($mbo_3_company && in_array('MBO III - C4', $mbo_3_company)): 
              echo displayMbo(3,$post,4);
              endif;
      echo displayReturnAndGeneralReporting(3,$return_report_post,$post);
      endif;
     ?>

     <?php 
     if($mbo_4): 
      if($mbo_4_company && in_array('MBO IV - C1', $mbo_4_company)): 
        echo displayMbo(4,$post,1);
        endif;
        if($mbo_4_company && in_array('MBO IV - C2', $mbo_4_company)): 
          echo displayMbo(4,$post,2);
          endif;
          if($mbo_4_company && in_array('MBO IV - C3', $mbo_4_company)): 
            echo displayMbo(4,$post,3);
            endif;
            if($mbo_4_company && in_array('MBO IV - C4', $mbo_4_company)): 
              echo displayMbo(4,$post,4);
              endif;
      echo displayReturnAndGeneralReporting(4,$return_report_post,$post);
      endif;
     ?>
  </div><!-- end of main -->
  
  <div id="footer">
    <div class="container">
      <p>2020 © Westcap Mgt. Ltd. | All Rights Reserved.</p>
    </div>
  </div>
  
  <script>
    
    $(document).ready(function(){ //makes first tab visible on load
      $('.tab-container .tab:first-child').addClass('active');
    });
    
    $('.tab-container .tab').click(function(){ //toggles tabs
      $(this).addClass('active').siblings('.tab').removeClass('active');
    });
    
    $('#header .hamburger').click(function(){ //mobile toggle
      $('#header').toggleClass('nav-open');
    });
    
  </script>
  
</body>
</html>