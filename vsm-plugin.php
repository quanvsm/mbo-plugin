<?php
/*
   Plugin Name: Import VSM
   Plugin URI: 
   description:  ViewSource Media Custom plugin
   Version: 1.8
   Author: ViewSource Media
   Author URI:
   License: 
*/

if (!defined('ABSPATH')) {
	exit;
}

session_start();
require 'vendor/autoload.php';
include_once($_SERVER['DOCUMENT_ROOT'] . "/wp-includes/wp-db.php");
require($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
require($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
require(ABSPATH . "wp-includes/pluggable.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

add_filter('option_active_plugins', function ($plugins) {
	$key = array_search('advanced-custom-fields/acf.php', $plugins);
	if ($key !== false) {
		unset($plugins[$key]);
	}
	return $plugins;
});

//initiate Template class for custom VSM template.
class PageTemplater
{

	private static $instance;
	protected $templates;

	public static function get_instance()
	{
		if (null == self::$instance) {
			self::$instance = new PageTemplater();
		}
		return self::$instance;
	}

	private function __construct()
	{
		$this->templates = array();
		if (version_compare(floatval(get_bloginfo('version')), '4.7', '<')) {
			add_filter(
				'page_attributes_dropdown_pages_args',
				array($this, 'register_project_templates')
			);
		} else {
			add_filter(
				'theme_page_templates',
				array($this, 'add_new_template')
			);
		}

		add_filter(
			'wp_insert_post_data',
			array($this, 'register_project_templates')
		);

		add_filter(
			'template_include',
			array($this, 'view_project_template')
		);

		$this->templates = array(
			'vsm-custom-template.php' => 'VSM',
		);
	}

	public function add_new_template($posts_templates)
	{
		$posts_templates = array_merge($posts_templates, $this->templates);
		return $posts_templates;
	}

	public function register_project_templates($atts)
	{
		$cache_key = 'page_templates-' . md5(get_theme_root() . '/' . get_stylesheet());

		$templates = wp_get_theme()->get_page_templates();
		if (empty($templates)) {
			$templates = array();
		}

		wp_cache_delete($cache_key, 'themes');
		$templates = array_merge($templates, $this->templates);

		wp_cache_add($cache_key, $templates, 'themes', 1800);

		return $atts;
	}

	public function view_project_template($template)
	{
		if (is_search()) {
			return $template;
		}

		global $post;

		if (!$post) {
			return $template;
		}

		if (!isset($this->templates[get_post_meta(
			$post->ID,
			'_wp_page_template',
			true
		)])) {
			return $template;
		}

		$filepath = apply_filters('page_templater_plugin_dir_path', plugin_dir_path(__FILE__));

		$file =  $filepath . get_post_meta(
			$post->ID,
			'_wp_page_template',
			true
		);

		if (file_exists($file)) {
			return $file;
		} else {
			echo $file;
		}

		return $template;
	}
}
add_action('plugins_loaded', array('PageTemplater', 'get_instance'));


// add_filter('theme_page_templates', 'pt_add_page_template_to_dropdown');
// add_filter('template_include', 'pt_change_page_template', 99);
// add_action('wp_enqueue_scripts', 'pt_remove_style' );

// /**
//  * Add page templates.
//  *
//  * @param  array  $templates  The list of page templates
//  *
//  * @return array  $templates  The modified list of page templates
//  */
// function pt_add_page_template_to_dropdown($templates)
// {
//     $templates[plugin_dir_path(__FILE__) . 'templates/page-template.php'] = __('Page Template From Plugin', 'text-domain');

//     return $templates;
// }

// /**
//  * Change the page template to the selected template on the dropdown
//  * 
//  * @param $template
//  *
//  * @return mixed
//  */
// function pt_change_page_template($template)
// {
//     if (is_page()) {
//         $meta = get_post_meta(get_the_ID());

//         if (!empty($meta['_wp_page_template'][0]) && $meta['_wp_page_template'][0] != $template) {
//             $template = $meta['_wp_page_template'][0];
//         }
//     }

//     return $template;
// }

// function pt_remove_style()
// {
//     // Change this "my-page" with your page slug
//     if (is_page('my-page')) {
//         $theme = wp_get_theme();

//         $parent_style = $theme->stylesheet . '-style'; 

//         wp_dequeue_style($parent_style);
//         wp_deregister_style($parent_style);
//         wp_deregister_style($parent_style . '-css');
//     }
// }

//Make sure ACF Pro was fully loaded to use all ACF functions.
add_action('plugins_loaded', 'run_VSM_plugin');

function run_VSM_plugin()
{
	// add options page.
	if (is_admin()) {
		add_action('admin_menu', 'vsm_import_client_data');
	}
	function vsm_import_client_data()
	{
		add_menu_page(
			'VSM Functions',
			'VSM Functions',
			'edit_posts',
			'import_client_data',
			'vsm_option_form',
			'dashicons-edit-large',
			21
		);
	}

	//include the form to the admin panel.
	function vsm_option_form()
	{
		include 'views/form/form.php';
	}

	//Security. only show user-data to admins.
	if (is_admin()) {
		add_action('init', 'custom_post_types');
	}
	function custom_post_types()
	{

		$users_data = array(
			'name'               => _x('User Data', 'Data general name'),
			'singular_name'      => _x('User Data', 'Data singular name'),
			'add_new'            => _x('Add New', 'add new'),
			'add_new_item'       => __('Add New Data', 'add new Data'),
			'edit_item'          => __('Edit Data', 'edit Data'),
			'new_item'           => __('New Data', 'new Data'),
			'all_items'          => __('All Data', 'all Data'),
			'view_item'          => __('View All Data', 'view Data'),
			'search_items'       => __('Search Data', 'search Data'),
			'not_found'          => __('No Data found', 'no Data found'),
			'not_found_in_trash' => __('No Data found in the trash', 'no Data found in trash'),
			'parent_item_colon'  => '',
			'menu_name'          => 'User Data'
		);
		$users_data_args = array(
			'labels'        => $users_data,
			'description'   => 'User Data',
			'public'        => false,
			'show_ui'       => true,
			'menu_position' => 20,
			'menu_icon' 	=> 'dashicons-book',
			'supports'      => array('title'),
			'show_in_rest'	=> 	false,
			'has_archive'   => false,
			'publicaly_queryable' => false,
			'query_var' => false,
			'rewrite' => array('slug' => 'user-data', 'with_front' => true)
		);

		register_post_type('user-data', $users_data_args);
	}

	//clear user data
	if (isset($_POST['clear_user_data']) && $_POST['clear_user_data'] == "Clear") {
		if ($_POST["data_type"] && $_POST['user_account_numbers']) {
			$invalid_account_numbers = [];
			$failed_account_numbers = [];
			$account_number_array = array_map('trim', explode(",", $_POST['user_account_numbers']));

			$user_contact_fields = ["name", "address", "contact_name_1", "contact_name_2", "phone_number", "email"];
			$mbo_fields = [
				"company_name", "units", "committed_capital", "contributed_capital", "cumulative_distributions",
				"net_asset_value", "net_gains_loss",
				"beginning_balance", "beginning_balance_date",
				"ending_balance", "ending_balance_date",
				"capital_calls", "distributions", "increase_decrease_in_value", "returns_type",
				"annual_statements", "annual_statement_year", "annual_statement_link",
				"cash_call", "cash_call_type", "cash_call_date", "capital_call_letter", "capital_call_letter_link",
				"irr", "simple_return", "notice_of_distribution", "notice_of_distribution_year", "notice_of_distribution_link",
				"tax_slips", "tax_title_name", "tax_file_link", "tax_year"
			];


			foreach ($account_number_array as $account_number) {
				global $wpdb;
				$post = get_page_by_title($account_number, OBJECT, 'user-data');
				if ($post) {
					foreach ($_POST["data_type"] as $type) {
						if ($type == "contact-info") {
							foreach ($user_contact_fields as $contact_field) {

								delete_field($contact_field, $post->ID);
							}
						} else {
							switch ($type) {
								case "mbo-1":
									$mbo = 1;
									break;
								case "mbo-2":
									$mbo = 2;
									break;
								case "mbo-3":
									$mbo = 3;
									break;
								case "mbo-4":
									$mbo = 4;
									break;
								default:
									$mbo = 0;
									$_SESSION['error'] = "Error! Please Try Again!";
									exit();
							}

							if ($mbo == 3 || $mbo == 4) {
								array_push($mbo_fields, "closing_binder", "closing_binder_title", "closing_binder_link");
							}

							for ($i = 1; $i <= 4; $i++) {
								$company = "c" . $i;
								$group_name = $company . '_mbo_' . $mbo;
								$current_post_id = $post->ID;

								foreach ($mbo_fields as $mbo_field) {
									$current_field_name = $company . "_" . $mbo_field . "_" . $mbo;
									$wpdb->get_results($wpdb->prepare(
										$deleted = "DELETE FROM `wp_postmeta` WHERE post_id = $current_post_id AND meta_key LIKE '%%$current_field_name%%';",
									));

									if (!$deleted) {
										$failed_account_numbers[] = $account_number;
									}
								}
							}
						}
					}
				} else {
					$invalid_account_numbers[] = $account_number;
				}
			}
			if (count($invalid_account_numbers) > 0) {
				$_SESSION['delete-warning'] = implode(",", $invalid_account_numbers) . " are invalid!";
			}
			if (count($failed_account_numbers) > 0) {
				$_SESSION['error'] = "Data was not deleted for these accounts: " . implode(",", $failed_account_numbers);
			}
			if (count($invalid_account_numbers) == 0 &&  count($failed_account_numbers) == 0) {
				$_SESSION['success'] = "Deleted successfully!";
			}
		} else {
			$_SESSION['error'] = "Please complete all required fields!";
		}
	}

	//Save user data
	if (isset($_POST['update'])) {
		if (isset($_SESSION['vsm_user_data_file_name'])) {
			$uploads_dir = dirname(__FILE__) . '/uploads/';
			// $tmp_name = $_FILES["csv_file"]["tmp_name"];

			$name = $_SESSION['vsm_user_data_file_name'];
			$spreadsheet = new Spreadsheet();

			$inputFileType = 'Xlsx';
			$inputFileName = $uploads_dir . $name;

			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);

			$worksheetData = $reader->listWorksheetInfo($inputFileName);

			$row = 0;

			foreach ($worksheetData as $worksheet) {
				$sheetName = $worksheet['worksheetName'];
				$reader->setLoadSheetsOnly($sheetName);
				$spreadsheet = $reader->load($inputFileName);
				$worksheet = $spreadsheet->getActiveSheet();
				$data = $worksheet->toArray();
				$user_data_count = count($data) - 1;

				if (count($data[0]) > 9) {
					for ($x = 1; $x < count($data); $x++) {

						if ($data[1][0]) {
							// $as_of =  date("F j, Y", (($data[1][0] - 25569) * 86400));
							$as_of =  $data[$x][0];
						} else {
							$as_of =  '';
						}
						$mbo = $data[$x][1];
						$company = $data[$x][2];
						$company_name = $data[$x][3];
						$account_number = $data[$x][4];
						$units = $data[$x][5];
						$committed_capital = $data[$x][6];
						$contributed_capital = $data[$x][7];
						$cumulative_distributions = $data[$x][8];
						$net_asset_value = $data[$x][9];
						$net_gain_on_contributed_capital = $data[$x][10];
						$beginning_value = $data[$x][11];
						// $beginning_value_date = date("M j, Y", (($data[$x][12] - 25569) * 86400));
						$beginning_value_date = $data[$x][12];
						$capital_calls = $data[$x][13];
						$increase_in_value = $data[$x][14];
						$distributions = $data[$x][15];
						$ending_value = $data[$x][16];
						// $ending_value_date = date("M j, Y", (($data[$x][17] - 25569) * 86400));
						$ending_value_date = $data[$x][17];
						$statement_year = $data[$x][18];
						$statement_link = $data[$x][19];

						// $cash_call_year = date("F j, Y", (($data[$x][20] - 25569) * 86400));
						$cash_call_year = $data[$x][20];
						$cash_call_link = $data[$x][21];

						//New Notice of Distribution
						$notice_of_distribution_year = $data[$x][22];
						$notice_of_distribution_link = $data[$x][23];

						$tax_slip_year = $data[$x][24];
						$tax_slip_link = $data[$x][25];

						$closing_binder_title = $data[$x][26];
						$closing_binder_link = $data[$x][27];

						$post_id = "";
						$post = get_page_by_title($account_number, OBJECT, 'user-data');

						if (isset($post) && !empty($post)) {
							$post_id = $post->ID;
						} else {
							$new_user_post_data = array(
								'post_title'    => $account_number,
								'post_status'   => 'publish',
								'post_type' => 'user-data',
							);
							$post_id = wp_insert_post($new_user_post_data);
						}

						$comp = "c" . $company;
						$group_key = $comp . '_mbo_' . $mbo;

						if ($mbo == 1) {
							$as_of_field_name = "as_of";
						} else {
							$as_of_field_name = "as_of_" . $mbo;
						}
						update_field($as_of_field_name, $as_of, $post_id);
						$value = array(
							$comp . '_company_name_' . $mbo => $company_name,
							$comp . '_units_' . $mbo => $units,
							$comp . '_committed_capital_' . $mbo => $committed_capital,
							$comp . '_contributed_capital_' . $mbo => $contributed_capital,
							$comp . '_cumulative_distributions_' . $mbo => $cumulative_distributions,
							$comp . '_net_asset_value_' . $mbo => $net_asset_value,
							$comp . '_net_gains_loss_' . $mbo => $net_gain_on_contributed_capital,
							$comp . '_beginning_balance_' . $mbo => $beginning_value,
							$comp . '_beginning_balance_date_' . $mbo => $beginning_value_date,
							$comp . '_ending_balance_' . $mbo => $ending_value,
							$comp . '_ending_balance_date_' . $mbo => $ending_value_date,
							$comp . '_capital_calls_' . $mbo => $capital_calls,
							$comp . '_increase_decrease_in_value_' . $mbo => $increase_in_value,
							$comp . '_distributions_' . $mbo => $distributions,
						);

						update_field($group_key, $value, $post_id);

						//update Annual Statement Repeater Field
						if (strlen($statement_link) > 0 && strlen($statement_year) > 0) {
							$exising_repeater_fields = array();
							if (have_rows($group_key, $post_id)) :
								while (have_rows($group_key, $post_id)) : the_row();

									if (have_rows($comp . '_annual_statements_' . $mbo, $post_id)) :
										while (have_rows($comp . '_annual_statements_' . $mbo, $post_id)) : the_row();
											$current_repeater = array(
												$comp . '_annual_statement_year_' . $mbo => get_sub_field($comp . '_annual_statement_year_' . $mbo, $post_id),
												$comp . '_annual_statement_link_' . $mbo => get_sub_field($comp . '_annual_statement_link_' . $mbo, $post_id)
											);
											array_push($exising_repeater_fields, $current_repeater);
										endwhile;
									endif;
								endwhile;
							else :
							// no rows found
							endif;

							$new_value  = array(
								$comp . '_annual_statement_year_' . $mbo => $statement_year,
								$comp . '_annual_statement_link_' . $mbo => $statement_link
							);
							array_push($exising_repeater_fields, $new_value);
							$repeater_key = $comp . '_annual_statements_' . $mbo;
							$value = array(
								$repeater_key => $exising_repeater_fields
							);
							update_field($group_key, $value, $post_id);
						}

						//update Notice of Distribution Repeater Field
						if (strlen($notice_of_distribution_link) > 0 && strlen($notice_of_distribution_year) > 0) {
							$exising_repeater_fields = array();
							if (have_rows($group_key, $post_id)) :
								while (have_rows($group_key, $post_id)) : the_row();

									if (have_rows($comp . '_notice_of_distribution_' . $mbo, $post_id)) :
										while (have_rows($comp . '_notice_of_distribution_' . $mbo, $post_id)) : the_row();
											$current_repeater = array(
												$comp . '_notice_of_distribution_year_' . $mbo => get_sub_field($comp . '_notice_of_distribution_year_' . $mbo, $post_id),
												$comp . '_notice_of_distribution_link_' . $mbo => get_sub_field($comp . '_notice_of_distribution_link_' . $mbo, $post_id)
											);
											array_push($exising_repeater_fields, $current_repeater);
										endwhile;
									endif;
								endwhile;
							else :
							// no rows found
							endif;

							$new_value  = array(
								$comp . '_notice_of_distribution_year_' . $mbo => $notice_of_distribution_year,
								$comp . '_notice_of_distribution_link_' . $mbo => $notice_of_distribution_link
							);
							array_push($exising_repeater_fields, $new_value);
							$repeater_key = $comp . '_notice_of_distribution_' . $mbo;
							$value = array(
								$repeater_key => $exising_repeater_fields
							);
							update_field($group_key, $value, $post_id);
						}

						if ($mbo > 1) {
							//update Cash Call Repeater Field
							if (strlen($cash_call_link) > 0 && strlen($cash_call_year) > 0) {
								$exising_repeater_fields = array();
								if (have_rows($group_key, $post_id)) :
									while (have_rows($group_key, $post_id)) : the_row();

										if (have_rows($comp . '_cash_call_' . $mbo, $post_id)) :
											while (have_rows($comp . '_cash_call_' . $mbo, $post_id)) : the_row();
												$current_repeater = array(
													$comp . '_cash_call_date_' . $mbo => get_sub_field($comp . '_cash_call_date_' . $mbo, $post_id),
													$comp . '_capital_call_letter_link_' . $mbo => get_sub_field($comp . '_capital_call_letter_link_' . $mbo, $post_id)
												);
												array_push($exising_repeater_fields, $current_repeater);
											endwhile;
										endif;
									endwhile;
								else :
								// no rows found
								endif;

								$new_value  = array(
									$comp . '_cash_call_date_' . $mbo => $cash_call_year,
									$comp . '_capital_call_letter_link_' . $mbo => $cash_call_link
								);
								array_push($exising_repeater_fields, $new_value);
								$repeater_key = $comp . '_cash_call_' . $mbo;
								$value = array(
									$repeater_key => $exising_repeater_fields
								);
								update_field($group_key, $value, $post_id);
							}

							if ($mbo >= 3) {
								//update Cash Call Repeater Field
								if (strlen($closing_binder_title) > 0 && strlen($closing_binder_link) > 0) {
									$exising_repeater_fields = array();
									if (have_rows($group_key, $post_id)) :
										while (have_rows($group_key, $post_id)) : the_row();

											if (have_rows($comp . '_closing_binder_' . $mbo, $post_id)) :
												while (have_rows($comp . '_closing_binder_' . $mbo, $post_id)) : the_row();
													$current_repeater = array(
														$comp . '_closing_binder_title_' . $mbo => get_sub_field($comp . '_closing_binder_title_' . $mbo, $post_id),
														$comp . '_closing_binder_link_' . $mbo => get_sub_field($comp . '_closing_binder_link_' . $mbo, $post_id)
													);
													array_push($exising_repeater_fields, $current_repeater);
												endwhile;
											endif;
										endwhile;
									else :
									// no rows found
									endif;

									$new_value  = array(
										$comp . '_closing_binder_title_' . $mbo => $closing_binder_title,
										$comp . '_closing_binder_link_' . $mbo => $closing_binder_link
									);
									array_push($exising_repeater_fields, $new_value);
									$repeater_key = $comp . '_closing_binder_' . $mbo;
									$value = array(
										$repeater_key => $exising_repeater_fields
									);
									update_field($group_key, $value, $post_id);
								}
							}
						}

						//update Tax Slip Repeater Field
						if (strlen($tax_slip_link) > 0 && strlen($tax_slip_year) > 0) {
							$exising_repeater_fields = array();
							if (have_rows($group_key, $post_id)) :
								while (have_rows($group_key, $post_id)) : the_row();

									if (have_rows($comp . '_tax_slips_' . $mbo, $post_id)) :
										while (have_rows($comp . '_tax_slips_' . $mbo, $post_id)) : the_row();
											$current_repeater = array(
												$comp . '_tax_year_' . $mbo => get_sub_field($comp . '_tax_year_' . $mbo, $post_id),
												$comp . '_tax_file_link_' . $mbo => get_sub_field($comp . '_tax_file_link_' . $mbo, $post_id)
											);
											array_push($exising_repeater_fields, $current_repeater);
										endwhile;
									endif;
								endwhile;
							else :
							// no rows found
							endif;

							$new_value  = array(
								$comp . '_tax_year_' . $mbo => $tax_slip_year,
								$comp . '_tax_file_link_' . $mbo => $tax_slip_link
							);
							array_push($exising_repeater_fields, $new_value);
							$repeater_key = $comp . '_tax_slips_' . $mbo;
							$value = array(
								$repeater_key => $exising_repeater_fields
							);
							update_field($group_key, $value, $post_id);
						}
						$row++;
					}
					if ($row == $user_data_count) {
						$_SESSION['success'] = 'Data Imported!';
					} else {
						$_SESSION['warning'] = $row . " / " . $user_data_count;
					}
				}
				// else {
				// 	$_SESSION['error'] = "Wrong File! This is user contact information file!";
				// }
			}
			$file_pointer = $uploads_dir = dirname(__FILE__) . '/uploads/' . $_SESSION['vsm_user_data_file_name'];
			unlink($file_pointer);
			unset($_SESSION['vsm_user_data_file_name']);
		}
	}


	//Save User Contact Info.
	if (isset($_POST['update_contact'])) {
		if (isset($_SESSION['vsm_user_contact_file_name'])) {
			$uploads_dir = dirname(__FILE__) . '/uploads/';

			$name = $_SESSION['vsm_user_contact_file_name'];
			$spreadsheet = new Spreadsheet();

			$inputFileType = 'Xlsx';
			$inputFileName = $uploads_dir . $name;

			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);

			$worksheetData = $reader->listWorksheetInfo($inputFileName);

			$row = 0;

			foreach ($worksheetData as $worksheet) {
				$sheetName = $worksheet['worksheetName'];
				$reader->setLoadSheetsOnly($sheetName);
				$spreadsheet = $reader->load($inputFileName);
				$worksheet = $spreadsheet->getActiveSheet();
				$data = $worksheet->toArray();
				$data_count = count($data) - 1;
				if (count($data[0]) < 9) {
					for ($x = 1; $x < count($data); $x++) {
						$account_number = $data[$x][0];
						$name = $data[$x][1];
						$address = $data[$x][2];
						$contact_name_1 = $data[$x][3];
						$contact_name_2 = $data[$x][4];
						$phone_number = $data[$x][5];
						$email = $data[$x][6];

						$post = get_page_by_title($account_number, OBJECT, 'user-data');

						if (isset($post) && !empty($post)) {
							update_field("name", $name, $post->ID);

							update_field("address", $address, $post->ID);

							update_field("contact_name_1", $contact_name_1, $post->ID);

							update_field("contact_name_2", $contact_name_2, $post->ID);

							update_field("phone_number", $phone_number, $post->ID);

							update_field("email", $email, $post->ID);

							$row++;
						} else {
							$new_user_post_data = array(
								'post_title'    => $account_number,
								'post_status'   => 'publish',
								'post_type' => 'user-data',
							);
							$_SESSION['error'] = "No user with this Account Number!";
						}
					}
					if ($row == $data_count) {
						$_SESSION['success'] = 'User Data Imported!';
					} else {
						$_SESSION['warning'] = $row . " / " . $data_count;
					}
				} else {
					$_SESSION['error'] = "Wrong File! This is user data file!";
				}
			}
			$file_pointer = $uploads_dir = dirname(__FILE__) . '/uploads/' . $_SESSION['vsm_user_contact_file_name'];
			unlink($file_pointer);
			unset($_SESSION['vsm_user_contact_file_name']);
		}
	}


	//Unlink uploaded file , uset file name session when client cancel the proess.
	if (isset($_POST['cancel'])) {
		if (isset($_SESSION['vsm_user_data_file_name'])) {
			$file_pointer = dirname(__FILE__) . '/uploads/' . $_SESSION['vsm_user_data_file_name'];
			unlink($file_pointer);
			unset($_SESSION['vsm_user_data_file_name']);
		} else if ($_SESSION['vsm_user_contact_file_name']) {
			$file_pointer = dirname(__FILE__) . '/uploads/' . $_SESSION['vsm_user_contact_file_name'];
			unlink($file_pointer);
			unset($_SESSION['vsm_user_contact_file_name']);
		}
	}
}
